# hackers-news-app

create root folder

use git clone to clone repo

create .env on the api folder with the following:

MONGO_INITDB_ROOT_USERNAME=wachu
MONGO_INITDB_ROOT_PASSWORD=wachu
DATABASE_NAME=HackersDB
DATABASE_PORT=27017
DATABASE_HOST=mongo
DATABASE_CONNECTION=mongodb
PORT=5000

to initialize the app run the command: docker compose up on the root folder

to save posts from the api to the database open localhost:5000/api
